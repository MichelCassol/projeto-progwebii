<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <h2>Seja Bem-Vindo</h2>

    <b>Nome: </b> {{ $contato->nome }} <br>
    <b>Email: </b> {{ $contato->email }} <br>
    <b>Cargo: </b> {{ $contato->cargo->nome }} <br>
    <b>Ativo: </b> @if($contato->ativo) SIM @else NÃO @endif <br>
    <b>Mensagem: </b> {{ $contato->mensagem }} <br>

</body>
</html>
