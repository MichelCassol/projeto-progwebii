@extends('layout.app')

@section('conteudo')
    <div class="row">
        <div class="col-sm-3">
            <div class="grupo-entrada">
                <label for="tipocartao">Cartão</label>
                <select id='setor' name='setor' class='campo'>
                    <option>Selecione</option>
                    @foreach ($setores as $setor)
                        <option>{{ $setores->nome }}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
@endsection