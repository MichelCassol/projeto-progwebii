<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Contact V8</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <meta name="csrf-token" content="{{ csrf_token() }}">

    <!--===============================================================================================-->
        <link rel="icon" type="image/png" href="{{ asset('images/icons/favicon.ico') }}"/>
    <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="{{ asset('vendor/bootstrap/css/bootstrap.min.css') }}">
    <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="{{ asset('fonts/font-awesome-4.7.0/css/font-awesome.min.css') }}">
    <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="{{ asset('fonts/Linearicons-Free-v1.0.0/icon-font.min.css') }}">
    <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="{{ asset('vendor/animate/animate.css') }}">
    <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="{{ asset('vendor/css-hamburgers/hamburgers.min.css') }}">
    <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="{{ asset('vendor/animsition/css/animsition.min.css') }}">
    <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="{{ asset('vendor/select2/select2.min.css') }}">
    <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="{{ asset('vendor/daterangepicker/daterangepicker.css') }}">
    <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="{{ asset('css/util.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('css/main.css') }}">
    <!--===============================================================================================-->
    </head>
    <body>

        @include('sweetalert::alert')

        @yield('conteudo')

        @yield('cadastroContato')

        <div id="dropDownSelect1"></div>

        <!--===============================================================================================-->
            <script src="{{ asset('vendor/jquery/jquery-3.2.1.min.js') }}"></script>
        <!--============={{ asset('') }}==================================================================================-->
            <script src="{{ asset('vendor/animsition/js/animsition.min.js') }}"></script>
        <!--============={{ asset('') }}==================================================================================-->
            <script src="{{ asset('vendor/bootstrap/js/popper.js') }}"></script>
            <script src="{{ asset('vendor/bootstrap/js/bootstrap.min.js') }}"></script>
        <!--============={{ asset('') }}==================================================================================-->
            <script src="{{ asset('vendor/select2/select2.min.js') }}"></script>
        <!--============={{ asset('') }}==================================================================================-->
            <script src="{{ asset('vendor/daterangepicker/moment.min.js') }}"></script>
            <script src="{{ asset('vendor/daterangepicker/daterangepicker.js') }}"></script>
        <!--============={{ asset('') }}==================================================================================-->
            <script src="{{ asset('vendor/countdowntime/countdowntime.js') }}"></script>
        <!--============={{ asset('') }}==================================================================================-->
            <script src="{{ asset('https://maps.googleapis.com/maps/api/js?key=AIzaSyAKFWBqlKAGCeS1rMVoaNlwyayu0e0YRes') }}"></script>
            <script src="{{ asset('js/map-custom.js') }}"></script>
        <!--===============================================================================================-->
            <script src="{{ asset('js/main.js') }}"></script>

            <script>

                function deletar(id) {
                    var csrf_token=$('meta[name="csrf-token"]').attr('content');
                    
                    Swal.fire({
                        title: 'Você tem certeza?',
                        text: 'Uma vez excluído o registro, você não poderá recuperá-lo mais.',
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Sim, deletar',
                        cancelButtonText: 'Cancelar',
                    })
                    .then((result) => {
                        if (result.value) {
                            $.ajax({
                                url: "{{ url('/contato') }}" + '/' + id,
                                type: "DELETE",
                                data: {'_method' : 'DELETE', '_token' : csrf_token},
                                success : function(data) {
                                    $( "#contato" ).load(window.location.href + " #contato" );
                                    Swal.fire({
                                        title: 'Registro excluído!',
                                        text: 'O registro foi excluído permanentemente!',
                                        type: 'success',
                                    });
                                },
                                error : function(data) {
                                    Swal.fire({
                                        title: 'Ocorreu um erro ...',
                                        text: 'Não foi possível excluir o registro. Favor entrar em contato com o administrador do sistema!',
                                        type: 'error',
                                        timer: '5000',
                                    })
                                }
                            });
                        }
                    });
                }  
            </script>
        
            <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="{{ asset('https://www.googletagmanager.com/gtag/js?id=UA-23581568-13') }}"></script>
        <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        
        gtag('config', 'UA-23581568-13');
        </script>    
    </body>
</html>