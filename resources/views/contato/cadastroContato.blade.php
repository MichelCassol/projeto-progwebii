@extends('layout.app')

@section('cadastroContato')
    
	<div class="container-contact100">
            <div class="contact100-map" id="google_map" data-map-x="40.722047" data-map-y="-73.986422" data-pin="images/icons/map-marker.png" data-scrollwhell="0" data-draggable="1"></div>
    
            <div class="wrap-contact100">
                <form class="contact100-form validate-form" method="POST" @if(isset($contato))action="/contato/{{ $contato->id }}" @else action="/contato" @endif  >
                    @csrf
                    <input type="hidden" name="_method" value="{{ isset($contato) ? 'PATCH' : 'POST' }}"/>
                    <span class="contact100-form-title">
                        Contact Us
                    </span>
    
                    <div class="wrap-input100 validate-input" data-validate="Name is required">
                        <input value="{{ $contato->nome ?? '' }}" class="input100" type="text" name="name" placeholder="Full Name">
                        <span class="focus-input100-1"></span>
                        <span class="focus-input100-2"></span>
                    </div>
    
                    <div class="wrap-input100 validate-input" data-validate = "Valid email is required: ex@abc.xyz">
                        <input value="{{ $contato->email ?? '' }}" class="input100" type="text" name="email" placeholder="Email">
                        <span class="focus-input100-1"></span>
                        <span class="focus-input100-2"></span>
                    </div>

                    <div class="form-group">
                        <select class="form-control" name="cargo" id="cargo">
                            <option value="">Selecione o cargo</option>
                            @foreach ($cargos as $cargo)
                                <option value="{{ $cargo->id }}" @isset($contato) @if($contato->cargo_id == $cargo->id)selected @endif @endisset()>
                                    {{ $cargo->nome }}
                                </option>
                            @endforeach
                        </select>
                    </div>

                    <div class="wrap-input100 validate-input" data-validate = "Message is required">
                        <textarea  class="input100" name="message" placeholder="Your Message">{{ $contato->mensagem ?? '' }}</textarea>
                        <span class="focus-input100-1"></span>
                        <span class="focus-input100-2"></span>
                    </div>
    
                    <div class="contact100-form-checkbox">
                        <input class="input-checkbox100" id="ckb1" type="checkbox" name="check-opcao" @isset($contato) @if($contato->ativo)checked @endif @endisset>
                        <label class="label-checkbox100" for="ckb1">
                            Send copy to my-email
                        </label>
                    </div>
    
                    <div class="container-contact100-form-btn">
                        <button class="contact100-form-btn"type="submit">
                            Send Email
                        </button>
                    </div>
                </form>
            </div>
        </div>
    
    
@endsection