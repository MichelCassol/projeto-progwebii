@extends('layout.app')

@section('conteudo')
    <table class="table">
        <thead class="thead-dark">
            <tr>
                <th scope="col">Codigo</th>
                <th scope="col">Nome</th>
                <th scope="col">Grupo</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($contatos as $contato)
                <tr>
                    <td scope="row">{{ $contato->id }}</td>
                    <td>{{ $contato->nome }}</td>
                    <td>{{ $contato->cargo_id }}</td>
                    
                    <td style="text-align: center; width: 30%">
                        <a href="/contato/{{ $contato->id }}/edit">
                            <button id="btn-editar" type="submit" class="btn btn-secondary btn-rounded w-sm">Editar</button>
                        </a>
                        <button type="button" class="btn btn-danger" onclick="deletar({{ $contato->id }})">Excluir</button>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection