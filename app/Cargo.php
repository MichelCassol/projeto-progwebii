<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cargo extends Model
{
    protected $table = 'tb_cargo';
    protected $fillable = ['nome'];
    
}
