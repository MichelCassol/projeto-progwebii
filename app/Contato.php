<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contato extends Model
{
    protected $table = 'tb_contato';
    protected $fillable = ['nome','email','mensagem','ativado','id_cargo'];


    function cargo(){
        return $this->belongsTo('App\Cargo');
    }
}
