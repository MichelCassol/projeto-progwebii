<?php

namespace App\Http\Controllers;

use App\Cargo;
use App\Contato;
use App\Mail\EmailCadastroContato;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use RealRashid\SweetAlert\Facades\Alert;


class ContatoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contatos = Contato::orderBy('id', 'ASC')->get();
        return view('contato.listaContato',['contatos' => $contatos]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cargos = Cargo::orderBy('id', 'ASC')->get();
        return view('contato.cadastroContato',['cargos' => $cargos]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $contato = new Contato();

        $contato->nome = $request->input('name');
        $contato->email = $request->input('email');
        $contato->cargo_id = $request->input('cargo');
        $contato->mensagem = $request->input('message');
        //$contato->ativo = $request->input('check-opcao');

        if ($request->input('check-opcao') == 'on') {
            $contato->ativo = true;
        } else{
            $contato->ativo = false;
        }

        if(isset($contato)){
            $contato->save();
            Alert::success('Registro salvo','O registro foi salvo com sucesso');
            Mail::to($contato->email)->send(new EmailCadastroContato($contato));
            return redirect('/contato');
        }else{
            Alert::error('Erro','Ocorreu um erro ao salvar');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //dd($id);
        $contato =  Contato::find($id);
        $cargos = Cargo::orderBy('nome', 'ASC')->get();;
        return view('contato.cadastroContato',['contato'=>$contato, 'cargos'=>$cargos]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $contato = Contato::find($id);

        $contato->nome = $request->input('name');
        $contato->email = $request->input('email');
        $contato->cargo_id = $request->input('cargo');
        $contato->mensagem = $request->input('message');

        if ($request->input('check-opcao') == 'on') {
            $contato->ativo = true;
        } else{
            $contato->ativo = false;
        }

        if(isset($contato)){
            $contato->save();
            Alert::success('Registro salvo', 'O registro foi salvo com sucesso');
            return redirect('/contato');
        }else{
            Alert::error('Erro!','Ocorreu um erro ao salvar');
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Contato::destroy($id);

        $contatos = Contato::orderBy('id', 'ASC')->get();
        return view('contato.listacontato',['contatos'=>$contatos]);
    }
}
